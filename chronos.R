library(ape)
cnt <- c()
loglik <- c()
par(mfrow=c(1,1))
for (i in seq(0,100,1)){
  tr <- read.tree("/Users/mathieu/Documents/UNI/Master/scripts_clean_2018/results_and_intermediate/RAxML_bestTree.coleo_busco_insecta_partitioned_constraint.nwk")
  tr <- root(tr, "SMELI")
  ultra <- tr
  ultra <- invisible(chronos(tr, 0))
  loglik <- c(loglik,attr(ultra,"ploglik"))
  cnt <- c(cnt,i)
}
print(max(loglik))
#print(cnt)

#plot(tr, type = "unrooted")
#plot(ultra)
#nodelabels(1-node.depth.edgelength(ultra)[21:length(node.?depth.edgelength(ultra))],bg="white")

adephaga <- c(1-node.depth.edgelength(ultra))[26:33]
polyphaga  <- c(1-node.depth.edgelength(ultra)[22:23],1-node.depth.edgelength(ultra)[34:38])


require("ape")
#tr = rtree(10) # generate a random, non-ultrametric tree
#pdf("clock-trees_from_chronos.pdf")
par(mfcol = c(2,2))
plot(tr, main = "original tree") # original tree
ultra <- chronos(tr, lambda = 0, model = "correlated") # correlated clock model, lambda parameter set to 0
##plot(ultra, main=attr(ultra,"ploglik"))
loglik <- c(loglik,attr(ultra,"ploglik"))
axisPhylo()
ultra <- chronos(tr, lambda = 0.1, model = "correlated") # correlated clock model, lambda parameter set to 0.1
##plot(ultra, main=attr(ultra,"ploglik"))
loglik <- c(loglik,attr(ultra,"ploglik"))
axisPhylo()
ultra <- chronos(tr, lambda = 1.0, model = "correlated") # correlated clock model, lambda parameter set to 1.0
##plot(ultra, main=attr(ultra,"ploglik"))
loglik <- c(loglik,attr(ultra,"ploglik"))
axisPhylo()
ultra <- chronos(tr, model = "discrete", control = chronos.control(nb.rate.cat = 1)) # strict clock model
##plot(ultra, main=attr(ultra,"ploglik"))
loglik <- c(loglik,attr(ultra,"ploglik"))
axisPhylo()
ultra <- chronos(tr, model = "relaxed", lambda = 0) # relaxed clock model, lambda parameter set to 0
#plot(ultra, main=attr(ultra,"ploglik"))
loglik <- c(loglik,attr(ultra,"ploglik"))
axisPhylo()
ultra <- chronos(tr, model = "relaxed", lambda = 0.1) # relaxed clock model, lambda parameter set to 0.1
#plot(ultra, main=attr(ultra,"ploglik"))
loglik <- c(loglik,attr(ultra,"ploglik"))
axisPhylo()
ultra <- chronos(tr, model = "relaxed", lambda = 1.0) # relaxed clock model, lambda parameter set to 1.0
plot(ultra, main=attr(ultra,"ploglik"))
write.tree(ultra, file = "/Users/mathieu/Documents/UNI/Master/scripts_clean_2018/results_and_intermediate/ultra.nwk")
loglik <- c(loglik,attr(ultra,"ploglik"))
axisPhylo()
#dev.off()
print(loglik)