#!cafe
#version
#date
load -i tmp.tab -t 8 -p 0.01
tree (((((OTAUR:0.1496069568,ACURT:0.1496069568):0.2033447823,((MVIOL:0.1341687148,TCAST:0.1341687148):0.1170733313,((LDECE:0.07472728561,AGLAB:0.07472728561):0.1133001357,(DPOND:0.08236720673,LTESS:0.08236720673):0.1056602146):0.06321462487):0.101709693):0.1519323003,APLAN:0.5048840395):0.3090671971,((CHYBR:0.3466174297,(CFRIG:0.1871081122,EAURE:0.1871081122):0.1595093175):0.2172320808,((NCLAV:0.2199563686,(HFLUV:0.08776932354,(GMARI:0.03884300877,DINEU:0.03884300877):0.04892631477):0.1321870451):0.1604650911,(CLATE:0.1729556492,AWRAS:0.1729556492):0.2074658106):0.1834280507):0.2501017261):0.1860487634,SMELI:1)
errormodel -sp OTAUR -model OTAUR.error.txt
errormodel -sp ACURT -model ACURT.error.txt
errormodel -sp MVIOL -model MVIOL.error.txt
errormodel -sp TCAST -model TCAST.error.txt
errormodel -sp LDECE -model LDECE.error.txt
errormodel -sp AGLAB -model AGLAB.error.txt
errormodel -sp DPOND -model DPOND.error.txt
errormodel -sp LTESS -model LTESS.error.txt
errormodel -sp APLAN -model APLAN.error.txt
errormodel -sp CHYBR -model CHYBR.error.txt
errormodel -sp CFRIG -model CFRIG.error.txt
errormodel -sp EAURE -model EAURE.error.txt
errormodel -sp NCLAV -model NCLAV.error.txt
errormodel -sp HFLUV -model HFLUV.error.txt
errormodel -sp GMARI -model GMARI.error.txt
errormodel -sp DINEU -model DINEU.error.txt
errormodel -sp CLATE -model CLATE.error.txt
errormodel -sp AWRAS -model AWRAS.error.txt
errormodel -sp SMELI -model SMELI.error.txt
lambda -s
report tmp_result