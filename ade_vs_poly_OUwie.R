library(stringr)
library(ape)
library(OUwie)
library(ggplot2)
library(phytools)

delta.AICc <- 2
par(mfrow=c(1,1))

n <- 0
a <- 0
p <- 0

ratio <- 4/3
diff <- 1

setwd('/Users/mathieu/Documents/')

tr <- read.tree("tree.nwk")

tr <- drop.tip(tr,c('SMELI'))

tr$node.label <- c('R',rep('P', 8),rep('A', 8))

plot(tr, show.node.label = T)

data <- read.delim("counts.tab")

data <- data[data$ID %in% c(68700, 57578, 13436, 24843, 74490, 14409, 92264, 56910),]

ids = c(118708,14409,50717,67695,74490,75372,75849,86020,86061,92264)

data[data==0] <- 1

print(data)

for(i in c(1:length(data[,1]))){
  
  cat('#',i,'/',length(data[,1]),'\n')
  cnt <- table(as.vector(t(data[i, 2:21])))
  
  
  dat <- data.frame(species=tr$tip.label,
                    group=c(rep("P",9),rep("A",9)),
                    nb=c(
                         data[i,]$OTAUR,
                         data[i,]$ACURT,
                         data[i,]$MVIOL,
                         data[i,]$TCAST,
                         data[i,]$LDECE,
                         data[i,]$AGLAB,
                         data[i,]$DPOND,
                         data[i,]$LTESS,
                         data[i,]$APLAN,
                         data[i,]$CHYBR,
                         data[i,]$CFRIG,
                         data[i,]$EAURE,
                         data[i,]$NCLAV,
                         data[i,]$HFLUV,
                         data[i,]$GMARI,
                         data[i,]$DINEU,
                         data[i,]$CLATE,
                         data[i,]$AWRAS
                         ))
  

  mod.bm1 <- 
    OUwie(tr, dat, model="BM1", simmap.tree=F, scaleHeight=T, root.station=F,
          mserr="none", diagn=F, quiet=T, warn=F)
  
  mod.bms <- invisible(
    OUwie(tr, dat, model="BMS", simmap.tree=F, scaleHeight=T, root.station=F,
          mserr="none", diagn=F, quiet=T, warn=F))        
  
  mod.ou1 <- invisible(
    OUwie(tr, dat, model="OU1", simmap.tree=F, scaleHeight=T, root.station=F,
          mserr="none", diagn=F, quiet=T, warn=F))
  
  mod.oum <- invisible(
    OUwie(tr, dat, model="OUM", simmap.tree=F, scaleHeight=T, root.station=F, 
          mserr="none", diagn=F, quiet=T, warn=F))
  
  mod.oum.rs <- invisible(
    OUwie(tr, dat, model="OUM", simmap.tree=F, scaleHeight=T, root.station=T, 
          mserr="none", diagn=F, quiet=T, warn=F))
  
  if(mod.oum.rs$AICc < mod.oum$AICc && mod.oum.rs$AICc > 0){
    mod.oum <- mod.oum.rs
    oum.rs <- T
    print('yep')
  }
  else{
    oum.rs <- F
    print('no')
  }
  
  mod.oumv <- invisible(
    OUwie(tr, dat, model="OUMV", simmap.tree=F, scaleHeight=T, root.station=F,
          mserr="none", diagn=F, quiet=T, warn=F))
  
  mod.oumv.rs <- invisible(
    OUwie(tr, dat, model="OUMV", simmap.tree=F, scaleHeight=T, root.station=T,
          mserr="none", diagn=F, quiet=T, warn=F))
  
  if(mod.oumv.rs$AICc < mod.oumv$AICc && mod.oumv.rs$AICc > 0){
    mod.oumv <- mod.oumv.rs
    oumv.rs <- T
  }
  else{
    oumv.rs <- F
  }
  
  h0.AICc <- min(abs(mod.bm1$AICc),abs(mod.bms$AICc),abs(mod.ou1$AICc))
  
  if(mod.oumv$AICc < mod.oum$AICc && mod.oumv$AICc > 0){
    h1 <- mod.oumv
    if(oumv.rs){
      shift = 1
    }
    else{
      shift = 0
    }
  }
  else{
    h1 <- mod.oum
    if(oum.rs){
      shift = 1
    }
    else{
      shift = 0
    }
  }
  
  if((h0.AICc - h1$AICc) >= delta.AICc && h1$AICc > 0){
    cat(data[i,1],round(mod.bm1$AICc, digits = 2), round(mod.bms$AICc, digits = 2), round(mod.ou1$AICc, digits = 2), round(mod.oumv$AICc, digits = 2), round(mod.oum$AICc, digits = 2), round(h0.AICc, digits = 2) ,round(h1$AICc, digits = 2), sep="\t")
    
    if((h1$theta[2-shift,1] < h1$theta[3-shift,1]/ratio) && (h1$theta[3-shift,1])-h1$theta[2-shift,1] >= diff){
      cat("\t")
      cat('POLY', sep="\t")
      cat("\t")
      cat('A',round(h1$theta[2-shift,1], digits = 2),
          'P',round(h1$theta[3-shift,1], digits = 2),
          #'R',round(h1$theta[4-shift,1], digits = 2),
          'Strength:',round(h1$solution[1,1],digits = 2), shift,'\n', sep="\t")
      p <- p+1
    }
    else if((h1$theta[3-shift,1] < h1$theta[2-shift,1]/ratio) && (h1$theta[2-shift,1])-h1$theta[3-shift,1] >= diff){
      cat("\t")
      cat('ADE ', sep="\t")
      cat("\t")
      cat('A',round(h1$theta[2-shift,1], digits = 2),
          'P',round(h1$theta[3-shift,1], digits = 2),
          #'R',round(h1$theta[4-shift,1], digits = 2),
          'Strength:',round(h1$solution[1,1],digits = 2), shift,'\n', sep="\t")
      a <- a+1
    }
    else{
      cat("\t")
      cat("TIE ", sep="\t")
      cat("\t")
      cat('A',round(h1$theta[2-shift,1], digits = 2),
          'P',round(h1$theta[3-shift,1], digits = 2),
          #'R',round(h1$theta[4-shift,1], digits = 2),
          'Strength:',round(h1$solution[1,1],digits = 2), shift,'\n', sep="\t")
      
      n <- n+1
    }
    
  }
  
  else{
    cat(data[i,1],round(mod.bm1$AICc, digits = 2), round(mod.bms$AICc, digits = 2), round(mod.ou1$AICc, digits = 2), round(mod.oumv$AICc, digits = 2), round(mod.oum$AICc, digits = 2), round(h0.AICc, digits = 2) ,round(h1$AICc, digits = 2), sep="\t")
    cat("\t")
    cat("NS  ",'\n')
    n <- n+1
  }
  
  cat('A',round(h1$theta[2-shift,1], digits = 2),
      'P',round(h1$theta[3-shift,1], digits = 2),
      'Strength:',round(h1$solution[1,1],digits = 2), shift,'\n', sep="\t")
  
}

cat('\nade: ', a)
cat('\npol: ', p)
cat('\nns: ', n)

cat('\t')
cat('\n')