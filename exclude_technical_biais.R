library(stringr)
library(ape)
library(phylolm)
library(qvalue)
library(reshape)
library(MCMCglmm)
library(ggplot2)


# Multiple plot function
#
# ggplot objects can be passed in ..., or to plotlist (as a list of ggplot objects)
# - cols:   Number of columns in layout
# - layout: A matrix specifying the layout. If present, 'cols' is ignored.
#
# If the layout is something like matrix(c(1,2,3,3), nrow=2, byrow=TRUE),
# then plot 1 will go in the upper left, 2 will go in the upper right, and
# 3 will go all the way across the bottom.
#
multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  library(grid)
  
  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)
  
  numPlots = length(plots)
  
  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols))
  }
  
  if (numPlots==1) {
    print(plots[[1]])
    
  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))
    
    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
      
      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}

par(mfrow=c(1,3))

nb <- data.frame(LEPIC=character(),AGLAB=character(),LDECE=character(),LTESS=character(),DPOND=character(),MVIOL=character(),TCAST=character(),OTAUR=character(),ACURT=character(),APLAN=character(),NCLAV=character(),CHYBR=character(),CBALD=character(),EAURE=character(),GMARI=character(),DINEU=character(),AWRAS=character(),CLATE=character(),HFLUV=character())
result <- read.table("~/cafe/250/all.cafe",skip = 9,header=T)
trees <- result$Newick[result$Family.wide.P.value <= 0.01]
for (t in trees){
  tr <- read.tree(text=paste(t,";",sep=""))
  coln <- colnames(nb)
  nb_new <- c(as.numeric(str_extract_all(tr$tip.label,"\\(?[0-9,.]+\\)?")))
  nb <- rbind(nb,nb_new)
  colnames(nb) <- coln
}
lse <- melt(nb[2:10])#, id=c("id","time"))
lse2 <- melt(c(nb[1],nb[11:19]))#, id=c("id","time"))
lse$grp <- "trans"
lse$grp[lse$variable == "TCAST"] <- "gen"
lse$grp[lse$variable == "OTAUR"] <- "gen"
lse$grp[lse$variable == "APLAN"] <- "gen"
lse$grp[lse$variable == "LDECE"] <- "gen"
lse$grp[lse$variable == "DPOND"] <- "gen"
lse$grp[lse$variable == "AGLAB"] <- "gen"

#lse$grp[lse$variable == "HFLUV"] <- "1kite"
#lse$grp[lse$variable == "DINEU"] <- "1kite"
#lse$grp[lse$variable == "GMARI"] <- "1kite"
#lse$grp[lse$variable == "AWRAS"] <- "1kite"
#lse$grp[lse$variable == "NCLAV"] <- "1kite"
#lse$grp[lse$variable == "CLATE"] <- "1kite"
#lse$grp[lse$variable == "CHYBR"] <- "1kite"
#lse$grp[lse$variable == "EAURE"] <- "1kite"
#lse$grp[lse$variable == "APLAN"] <- "1kite"
#boxplot(value ~ grp,data = lse,las=2)
#boxplot(value ~ variable,data = lse,las=2)
#boxplot(value ~ variable,data = lse2,las=2)

bp <- ggplot(lse, aes(x=grp, y=value)) +
  geom_boxplot() +scale_x_discrete(labels=c("GEN. ", "TRANS.")) +
  xlab("") + ylab("Gene family size") + ggtitle("Polyphaga") + theme(axis.text.x = element_text(angle = 90, hjust = 1))+ scale_y_continuous(limits = c(0, 20))
bp2 <- ggplot(lse, aes(x=variable, y=value)) +
  geom_boxplot() + #scale_x_discrete(labels=c("Adephaga", "Polyphaga")) +
  xlab("") + ylab("") + ggtitle("Polyphaga") + theme(axis.text.x = element_text(angle = 90, hjust = 1))+ scale_y_continuous(limits = c(0, 20))

bp3 <- ggplot(lse2, aes(x=L1, y=value)) +
  geom_boxplot() + #+scale_x_discrete(labels=c("Adephaga", "Polyphaga")) +
  xlab("") + ylab("") + ggtitle("Adephaga/Myxophaga") + theme(axis.text.x = element_text(angle = 90, hjust = 1))+ scale_y_continuous(limits = c(0, 20))

        
multiplot(bp,bp2,bp3,cols=3)
